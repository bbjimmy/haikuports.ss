SUMMARY="An integrated development environment for yab"
DESCRIPTION="
Yab allows fast prototyping with simple and clean code. yab contains a large \
number of BeAPI specific commands for GUI creation and much, much more.

yab wouldn't be complete without the yab-IDE, a powerful development \
environment, which of course is programmed in yab itself.
"
HOMEPAGE="https://github.com/bbjimmy/Yab"
SRC_URI="https://github.com/bbjimmy/Yab/archive/yab_ide2.2.4.tar.gz"
CHECKSUM_SHA256="4be806c0daae4f842e9232a1af9ceb5ae8c9a83b9c9ab44cddf1f70e29ec0e32"

REVISION="2"
LICENSE="Artistic"
COPYRIGHT="
	1995-2006 Marc-Oliver Ihm (yabasic)
	2006-2009 Jan Bungeroth (yab improvements)
	2013 Jim Saxton (yab improvements)
	"

ARCHITECTURES="x86_gcc2 x86 ?x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"
PROVIDES="
	yab_ide$secondaryArchSuffix = $portVersion
	app:yab_IDE = $portVersion
	"
REQUIRES="
	haiku$secondaryArchSuffix
	lib:libncurses$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	yab$secondaryArchSuffix >= 1.7.4
	devel:libncurses$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel
	devel:libncurses$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:bison
	cmd:flex
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:mkdepend
	cmd:unzip
	"
SOURCE_DIR="Yab-yab_ide2.2.4"

BUILD()
{
	cd src
	make $jobArgs BUILDHOME=`finddir B_SYSTEM_DEVELOP_DIRECTORY`
	unzip -o App_YAB.zip
	copyattr App_YAB yab
	cd ../yab-IDE/BuildFactory
	gcc -o yab-compress yab-compress.c -lz
}

INSTALL()
{
	mkdir -p $binDir
	mkdir -p tmp
	mkdir -p tmp/buildfactory

	cp -r src/* tmp/buildfactory
	cp -r yab-IDE/BuildFactory/* tmp/buildfactory/
	unzip -o tmp/buildfactory/parts/yabstuff.zip -d tmp/buildfactory/parts
	cp yab-IDE/src/yab-IDE.yab  tmp/buildfactory/

	pushd tmp/buildfactory
	BuildFactory.yab yab-IDE yab-IDE.yab application/x-vnd.yab-IDE
	popd

	mkdir -p $appsDir
	cp -r yab-IDE/ $appsDir/
	cp  tmp/buildfactory/yab-IDE $appsDir/yab-IDE/
	setversion $appsDir/yab-IDE/yab-IDE -app 2 2 4 g 0 -short "yab IDE" -long "An integrated development environment for yab"
	cp yab-IDE/src/yab-IDE.yab $appsDir/yab-IDE/src/
	addAppDeskbarSymlink $appsDir/yab-IDE/yab-IDE "Yab IDE"
}
